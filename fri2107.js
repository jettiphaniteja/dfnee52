var x= 'Hello world';
console.log(x.length);
console.log(x.substring(6));
console.log(x.includes("world"));
const arr=["java","python","HTML","CSS"];
const result =arr.filter(word => word.length >4);
console.log(result);
console.log(arr.length)

//Arrow function- Arrow functions{()=>} are a clear and concise method of writing normal/regular Javascript functions in a more accurate and shorter way.
//it won't allow duplicate functio
//const ...(num)


//higher order function-  for each an every element to perform an action.
//map&filter=it will reduce the code to short way,reduce=to reduce to a single value 
//cube
const arr1=[1,2,3,4,5]
let cube_arr = arr1.map((num)=>num*num*num);
console.log(cube_arr)
//odd
let oddnum_arr= arr1.filter((num)=>num%2!=0);
console.log(oddnum_arr)
let prod_arr = arr1.reduce((sum,num)=>sum*num,1)
console.log(prod_arr)
const arr2=["hi","helo","bye"]
let words= arr2.filter(word=> word.length>3)
console.log(words)

//doms-