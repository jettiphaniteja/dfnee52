let i=1;
while(i<=10){
    console.log(i);
    i++;
}
function sumofN(limit){
let num =1;
let sum=0;
while(num<=limit){
    sum =sum+num;
    num++;
}
return sum
}
let result=sumofN(10);
console.log("the sum of n numbers is "+result);//concatination
console.log(`the sum of n numbers is ${result}`);// string interpolation



//write a program to reverse the given numb
function reverseofnum(n){
    let reversednum = 0;
    while(n!=0){
        reversednum = (reversednum*10)+n%10
        n=Math.floor(n/10);
    }
    return reversednum
}
console.log(reverseofnum(87547));

//count
function countofdigits(num1){
    let count =0;
    while(num1!=0){
        count++;
        num1=Math.floor(num1/10);
    }
    return count;
}
console.log(countofdigits(58369))


//sum of digits in a given num
function sumOfdigit(N){
    let sum1=0;
    while(N!=0){
    sum1=sum1+N%10;
    N=Math.floor(N/10);
    }
    return sum1;
}
console.log(sumOfdigit(123))


//reverse of a string hello
function reverseStr(str){
    return str.split('').reverse().join(' ')
}
console.log(reverseStr("hello"));